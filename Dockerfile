FROM python:3.6

RUN pip install -r requirements.txt

RUN python manage.py migrate

RUN python manage.py createsuperuser

RUN python manage.py runserver

EXPOSE 8000