from django.contrib import admin
from .models import Canal, Cursos, Videos


class FilterCursos(admin.ModelAdmin):
    list_filter = ["created_at", "categorias"]
    search_fields = ['titulo', 'conteudo']
    list_per_page = 10


# Register your models here.
admin.site.register(Canal)
admin.site.register(Cursos, FilterCursos)
admin.site.register(Videos)
