from django.contrib import admin
from .models import Perguntas, Niveis, Assuntos


class Filter(admin.ModelAdmin):
    list_filter = ["created_at", "assunto", "niveis"]
    search_fields = ["titulo", "conteudo"]
    list_per_page = 10


# Register your models here.
admin.site.register(Perguntas, Filter)
admin.site.register(Niveis)
admin.site.register(Assuntos)
