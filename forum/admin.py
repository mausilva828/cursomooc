from django.contrib import admin
from .models import Respostas, Topicos


class FilterTopicos(admin.ModelAdmin):
    list_filter = ["created_at", "categorias"]
    search_fields = ['titulo', 'conteudo']
    list_per_page = 10


class FilterRespostas(admin.ModelAdmin):
    list_filter = ["created_at"]
    list_per_page = 10


# Register your models here.
admin.site.register(Topicos, FilterTopicos)
admin.site.register(Respostas, FilterRespostas)
